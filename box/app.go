package main

import (
	"github.com/spf13/cobra"
	"strings"
	"treasbox/box/config"
	"treasbox/box/constants"
	"treasbox/box/context"
	"treasbox/box/executors"
	"treasbox/box/model"
)

func registerExecutors() {
	manager := model.GetExecutorManager()
	manager.Load("python[test-executor]", &executors.PythonExecutor{ModuleName: "test-executor"})
	manager.Load("shell", &executors.ShellExecutor{})
}

func preRunFunc(cmd *cobra.Command, args []string) {
	config.GetConfig().Load()
	registerExecutors()
}
func runFunc(c *cobra.Command, args []string) {
	parser := model.GetDefaultCommandParser()
	ctx := context.GetContext()
	ctx.Init(commandType(c), silent, workDir, pattern, ignorePattern)
	cmd := parser.Parse(ctx.Type(), args)
	manager := model.GetExecutorManager()
	ctx.Working(func(workDir string) {
		manager.RunAll(workDir, cmd)
	})
}

func commandType(c *cobra.Command) constants.CommandType {
	if strings.Index(c.Use, string(constants.TaskType)) != -1 {
		return constants.TaskType
	}
	return constants.BundleType
}

package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
	"path/filepath"
	"treasbox/box/config"
)

var workDir string
var pattern string
var ignorePattern string
var silent bool
var rootCmd = &cobra.Command{
	Use:               "treas",
	Short:             "Treas is a tool set for efficiency",
	Long:              `The treasure box to help improve the efficiency of projects`,
	CompletionOptions: cobra.CompletionOptions{DisableDefaultCmd: true},
	PreRun:            preRunFunc,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Use treas -h or --help for help.")
	},
}
var bundleCmd = &cobra.Command{
	Use:    "bundle [string to bundle]",
	Short:  "Execute the bundled set of tasks",
	Long:   `Bundle name according to the configuration file[treas.yaml].`,
	Args:   cobra.MaximumNArgs(1),
	PreRun: preRunFunc,
	Run:    runFunc,
}

var taskCmd = &cobra.Command{
	Use:    "task [string to task]",
	Short:  "Set of execution tasks",
	Long:   `Task name according to the configuration file[treas.yaml].`,
	Args:   cobra.MaximumNArgs(1),
	PreRun: preRunFunc,
	Run:    runFunc,
}

var configCmd = &cobra.Command{
	Use:    "config [string to config]",
	Short:  "Set of execution tasks",
	Long:   `Task name according to the configuration file[treas.yaml].`,
	Args:   cobra.MaximumNArgs(0),
	PreRun: preRunFunc,
	Run: func(cmd *cobra.Command, args []string) {
		filePath := filepath.Join(config.GetConfig().ExecuteDir, "treas.yaml")
		fmt.Println("nvim " + filePath)
		// 读取整个文件的内容
		content, err := os.ReadFile(filePath)
		if err != nil {
			log.Fatal(err)
		}
		// 打印文件内容
		fmt.Println(string(content))
	},
}

func main() {
	rootCmd.PersistentFlags().StringVarP(&workDir, "dir", "d", "", "workDir according to the configuration file[treas.yaml]")
	rootCmd.PersistentFlags().StringVarP(&pattern, "pattern", "p", "", "pattern according to the configuration file[treas.yaml]")
	rootCmd.PersistentFlags().StringVarP(&ignorePattern, "ignorePattern", "e", "", "ignore pattern according to the configuration file[treas.yaml]")
	rootCmd.PersistentFlags().BoolVarP(&silent, "silent", "n", false, "silent to execute view")
	rootCmd.AddCommand(taskCmd)
	rootCmd.AddCommand(bundleCmd)
	rootCmd.AddCommand(configCmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

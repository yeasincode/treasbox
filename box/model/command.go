package model

import "strings"

type Argument struct {
	Name  string
	Value string
}
type Command struct {
	Cmd  string
	Args []string
}

func (cmd Command) String() string {
	return cmd.Cmd + " " + strings.Join(cmd.Args, " ")
}

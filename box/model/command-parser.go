package model

import (
	"fmt"
	"os"
	"regexp"
	"strings"
	config2 "treasbox/box/config"
	"treasbox/box/constants"
)

var pattern = regexp.MustCompile(`\w+\s*`)

type CommandParser interface {
	Parse(cmd []string) Command
}

type DefaultCommandParser struct {
	CommandParser
}

func GetDefaultCommandParser() *DefaultCommandParser {
	return &DefaultCommandParser{}
}

func (parser *DefaultCommandParser) Parse(commandType constants.CommandType, args []string) []Command {
	if len(args) == 0 {
		fmt.Println("arguments can not be zero size")
		os.Exit(1)
	}
	name := args[0]
	commands := make([]Command, 0)
	config := config2.GetConfig()

	if commandType == constants.TaskType {
		items := config.Tasks[name]
		for _, item := range items {
			tokens := pattern.FindAllString(item, -1)
			commands = append(commands, buildCommand(tokens))
		}
	}

	return commands
}

func buildCommand(tokens []string) Command {
	for i, token := range tokens {
		tokens[i] = strings.TrimSpace(token)
	}
	command := Command{Cmd: tokens[0], Args: tokens[1:]}
	return command
}

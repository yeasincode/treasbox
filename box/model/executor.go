package model

import (
	"sync"
	"treasbox/box/context"
	"treasbox/box/log"
)

type Executor interface {
	Run(workDir string, command Command) ActionResult
}

type ExecutorManager interface {
	Context() context.Context
	Load(name string, executor Executor)
	UnLoad(name string)
	RunAll(workDir string, commands []Command)
}

type DefaultExecutorManager struct {
	ExecutorManager
	executorMap map[string]Executor
}

var defaultExecutorManagerInstance *DefaultExecutorManager
var defaultExecutorManagerOnce sync.Once

func GetExecutorManager() ExecutorManager {
	defaultExecutorManagerOnce.Do(func() {
		defaultExecutorManagerInstance = new(DefaultExecutorManager)
		defaultExecutorManagerInstance.executorMap = make(map[string]Executor)
	})
	return defaultExecutorManagerInstance
}

func (m *DefaultExecutorManager) Load(name string, executor Executor) {
	m.executorMap[name] = executor
}
func (m *DefaultExecutorManager) UnLoad(name string) {
	m.executorMap[name] = nil
}
func (m *DefaultExecutorManager) RunAll(workDir string, commands []Command) {
	ctx := context.GetContext()
	log.GLog.Info("work into[" + workDir + "]")
	for _, command := range commands {
		log.GLog.Info("run command[" + command.String() + "]")
		for name, executor := range m.executorMap {
			log.GLog.Info("run executor[" + name + "]")
			if ctx.IsSilent() {
				continue
			}
			result := executor.Run(workDir, command)
			result.Do()
		}
	}
}

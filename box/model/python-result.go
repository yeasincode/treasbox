package model

import "encoding/json"

// PythonResult represents the result with a type, name, and data.
type PythonResult struct {
	Typ  int         // 1. 数据 (Data) 2. 函数 (Function)
	Name string      // 类型为数据时，这个为具体序列化的类；类型为函数时，这个为函数名，参数则为数据部分
	Data interface{} // Data can be any type
}

// NewPythonResult is a constructor function to create a new Do.
func NewPythonResult(typ int, name string, data interface{}) *PythonResult {
	return &PythonResult{
		Typ:  typ,
		Name: name,
		Data: data,
	}
}

func (r *PythonResult) FromPythonJSON(jsonStr string) error {
	err := json.Unmarshal([]byte(jsonStr), r)
	if err != nil {
		return err
	}
	return nil
}

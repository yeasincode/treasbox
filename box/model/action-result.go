package model

import (
	"fmt"
	"os"
)

type ActionResult interface {
	Do()
}

type RenderActionResult struct {
	Msg string
}

func (receiver *RenderActionResult) Do() {
	fmt.Println(receiver.Msg)
	os.Exit(1)
}

func (receiver *RenderActionResult) Action() {
	return
}

type QueueActionResult struct {
	ActionResult
	Msg string
}

func (receiver *QueueActionResult) Do() {
	queue := GetQueue()
	queue.Enqueue(receiver.Msg)
}

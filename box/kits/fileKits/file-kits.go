package fileKits

import (
	"io"
	"os"
	"treasbox/box/log"
)

func EnsurePath(path string) {
	// 检查路径是否存在
	if _, err := os.Stat(path); os.IsNotExist(err) {
		// 路径不存在，创建路径
		err := os.MkdirAll(path, 0755) // 创建路径，0755 表示权限
		if err != nil {
			log.GLog.Error("创建路径失败:", err)
			os.Exit(1)
		}
	} else if err != nil {
		log.GLog.Error("创建路径失败:", err)
		os.Exit(1)
	}
}

// AppendToFile appends the given data to the specified file.
func AppendToFile(filename string, data string) error {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(data)
	if err != nil {
		return err
	}

	return nil
}

// ReadFromFile reads the content of the specified file and returns it as a string.
func ReadFromFile(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return "", err
	}

	return string(content), nil
}

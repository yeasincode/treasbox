package constants

import (
	"runtime"
)

const OsWin = 1
const OsLinux = 2
const OsDarwin = 3

func OsType() int {
	switch runtime.GOOS {
	case "windows":
		return OsWin
	case "linux":
		return OsLinux
	case "darwin":
		return OsDarwin
	}
	return -1
}

package constants

type CommandType string

const BundleType CommandType = "bundle"
const TaskType CommandType = "task"

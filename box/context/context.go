package context

import (
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"treasbox/box/config"
	"treasbox/box/constants"
	"treasbox/box/kits/fileKits"
	"treasbox/box/log"
)

type Context struct {
	commandType   constants.CommandType
	dir           string
	matchPattern  string
	ignorePattern string
	silent        bool
	workDirs      []string
	currentDir    string
	ConfigDir     string
	LogDir        string
	ExecutorsDir  string
}

func (c *Context) Type() constants.CommandType {
	return c.commandType
}

func (c *Context) BaseType() string {
	return c.dir
}
func (c *Context) IsSilent() bool {
	return c.silent
}

func (c *Context) WorkDirs() []string {
	return c.workDirs
}
func (c *Context) CurrentDir() string {
	return c.currentDir
}

var contextInstance *Context
var contextOnce sync.Once

func GetContext() *Context {
	contextOnce.Do(func() {
		contextInstance = &Context{}
	})
	return contextInstance
}

func (c *Context) Init(commandType constants.CommandType, silent bool, workDir, matchPattern, ignorePattern string) {
	c.commandType = commandType
	cfg := config.GetConfig()
	c.dir = cfg.Dir
	if workDir != "" {
		c.dir = workDir
	}
	c.matchPattern = matchPattern
	c.ignorePattern = ignorePattern
	c.silent = silent
	c.initWorkDir()
	c.initRuntime()
}

func (c *Context) initWorkDir() {
	c.workDirs = make([]string, 0)
	if strings.TrimSpace(c.matchPattern) == "" && strings.TrimSpace(c.ignorePattern) == "" {
		c.workDirs = append(c.workDirs, c.dir)
		return
	}

	var pathRegex, ignoreRegex *regexp.Regexp

	pathRegex, err := regexp.Compile(c.matchPattern)
	if err != nil {
		log.GLog.Error("initWorkDir-pathRegex", err.Error())
		os.Exit(1)
	}

	if len(c.ignorePattern) != 0 {
		ignoreRegex, err = regexp.Compile(c.ignorePattern)
		if err != nil {
			log.GLog.Error("initWorkDir-ignoreRegex", err.Error())
			os.Exit(1)
		}
	}

	// 打开目录
	dir, err := os.Open(c.dir)
	if err != nil {
		log.GLog.Error("initWorkDir-open dir", err.Error())
		os.Exit(1)
	}
	defer dir.Close()

	// 读取目录内容
	files, err := dir.Readdir(-1)
	if err != nil {
		log.GLog.Error("initWorkDir-read dir", err.Error())
		os.Exit(1)
	}

	// 遍历目录内容并打印子目录路径
	for _, file := range files {
		if file.IsDir() {
			targetPath := filepath.Join(c.dir, file.Name())
			skip := pathRegex != nil && !pathRegex.MatchString(file.Name())

			if !skip {
				skip = ignoreRegex != nil && ignoreRegex.MatchString(file.Name())
			}
			if skip {
				continue
			}

			c.workDirs = append(c.workDirs, targetPath)
		}
	}

	if len(c.workDirs) == 0 {
		c.workDirs = append(c.workDirs, c.dir)
	}
}

func (c *Context) Working(exe func(workDir string)) {
	for _, dir := range c.workDirs {
		c.currentDir = dir
		exe(dir)
	}
}

func (c *Context) initRuntime() {
	c.initRuntimeDir()
	c.initRunTimeExecutors()
}

func (c *Context) initRuntimeDir() {
	cfg := config.GetConfig()
	configDir := filepath.Join(cfg.ExecuteDir, cfg.ConfigDir)
	fileKits.EnsurePath(configDir)
	c.ConfigDir = configDir
	logDir := filepath.Join(configDir, cfg.LogDir)
	fileKits.EnsurePath(logDir)
	c.LogDir = logDir
	executorDir := filepath.Join(configDir, cfg.ExecutorsDir)
	fileKits.EnsurePath(executorDir)
	c.ExecutorsDir = executorDir
	executorPythonDir := filepath.Join(executorDir, "python")
	fileKits.EnsurePath(executorPythonDir)
}

func (c *Context) initRunTimeExecutors() {
	c.initRuntimePythonExecutors()
}

func (c *Context) initRuntimePythonExecutors() {

}

package config

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
	"sync"
)

type Config struct {
	Dir              string                       `yaml:"dir"`
	Bundles          map[string][]string          `yaml:"bundles"`
	Tasks            map[string][]string          `yaml:"tasks"`
	ExecutorSettings map[string]map[string]string `yaml:"executor-settings"`
	ConfigDir        string                       `yaml:"configDir"`
	LogDir           string                       `yaml:"logDir"`
	ExecutorsDir     string                       `yaml:"executorsDir"`
	ExecuteDir       string
}

// 单例实例和 sync.Once 变量
var configInstance *Config
var configOnce sync.Once

// GetConfig 获取配置的单例实例
func GetConfig() *Config {
	configOnce.Do(func() {
		configInstance = new(Config)
	})
	return configInstance
}

func (c *Config) Load() {
	executable, err := os.Executable()
	if err != nil {
		return
	}
	c.ExecuteDir = filepath.Dir(executable)
	cn := filepath.Join(c.ExecuteDir, "treas.yaml")
	dataBytes, err := os.ReadFile(cn)
	if err != nil {
		fmt.Println("Error reading configuration file：", err)
		return
	}
	err = yaml.Unmarshal(dataBytes, c)
	if err != nil {
		fmt.Println("Error parsing configuration file：", err)
		return
	}
}

package log

import (
	"fmt"
	"time"
)

// Level 定义日志级别类型
type Level int

const (
	INFO Level = iota
	WARNING
	ERROR
)

var GLog = &Log{level: INFO}

// Log 定义一个日志结构体
type Log struct {
	level Level
}

// log 输出日志信息
func (l *Log) log(level Level, format string, v ...interface{}) {
	if level >= l.level {
		prefix := l.getPrefix(level)
		message := fmt.Sprintf(format, v...)
		fmt.Printf("%s %s\n", prefix, message)
	}
}

// getPrefix 获取日志前缀
func (l *Log) getPrefix(level Level) string {
	timestamp := time.Now().Format("2006-01-02 15:04:05")
	levelStr := ""
	switch level {
	case INFO:
		levelStr = "INFO"
	case WARNING:
		levelStr = "WARNING"
	case ERROR:
		levelStr = "ERROR"
	default:
		levelStr = "UNKNOWN"
	}
	return fmt.Sprintf("[TreasBox]%s [%s]", timestamp, levelStr)
}

// Info 输出信息日志
func (l *Log) Info(format string, v ...interface{}) {
	l.log(INFO, format, v...)
}

// Warning 输出警告日志
func (l *Log) Warning(format string, v ...interface{}) {
	l.log(WARNING, format, v...)
}

// Error 输出错误日志
func (l *Log) Error(format string, v ...interface{}) {
	l.log(ERROR, format, v...)
}

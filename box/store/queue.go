package store

import (
	"database/sql"
	"errors"
	"path/filepath"
	"sync"
	"treasbox/box/config"
	"treasbox/box/log"
)

type Queue struct {
	db *sql.DB
}

var queueInstance *Queue
var queueOnce sync.Once

// GetQueue 获取队列的单例实例
func GetQueue() *Queue {
	queueOnce.Do(func() {
		path := filepath.Join(config.GetConfig().ConfigDir, "box.db") // 修改为实际的配置路径
		queueInstance = NewQueue(path)
	})
	return queueInstance
}

// NewQueue creates a new queue
func NewQueue(dbPath string) *Queue {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		log.GLog.Error("new queue: %v", err)
	}

	// Create the queue table if it doesn't exist
	createTableSQL := `CREATE TABLE IF NOT EXISTS queue (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        value TEXT
    );`
	_, err = db.Exec(createTableSQL)
	if err != nil {
		log.GLog.Error("new queue: %v", err)
	}

	return &Queue{db: db}
}

// Enqueue adds an element to the queue
func (q *Queue) Enqueue(value string) {
	insertSQL := `INSERT INTO queue (value) VALUES (?)`
	_, err := q.db.Exec(insertSQL, value)
	if err != nil {
		log.GLog.Error("enqueue: %v", err)
	}
}

// Dequeue removes and returns the front element of the queue
func (q *Queue) Dequeue() string {
	var id int
	var value string

	tx, err := q.db.Begin()
	if err != nil {
		log.GLog.Error("dequeue: %v", err)
	}

	// Find the front element
	row := tx.QueryRow(`SELECT id, value FROM queue ORDER BY id LIMIT 1`)
	err = row.Scan(&id, &value)
	if err != nil {
		tx.Rollback()
		if errors.Is(err, sql.ErrNoRows) {
			return ""
		}
		log.GLog.Error("dequeue: %v", err)
	}

	// Remove the front element
	_, err = tx.Exec(`DELETE FROM queue WHERE id = ?`, id)
	if err != nil {
		tx.Rollback()
		log.GLog.Error("dequeue: %v", err)
	}

	err = tx.Commit()
	if err != nil {
		log.GLog.Error("dequeue: %v", err)
	}

	return value
}

// Peek returns the front element without removing it
func (q *Queue) Peek() string {
	var value string

	row := q.db.QueryRow(`SELECT value FROM queue ORDER BY id LIMIT 1`)
	err := row.Scan(&value)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return ""
		}
		log.GLog.Error("peek: %v", err)
	}

	return value
}

// Close closes the database connection
func (q *Queue) Close() error {
	return q.db.Close()
}

package executors

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"treasbox/box/constants"
	"treasbox/box/log"
	"treasbox/box/model"
)

type ShellExecutor struct {
	model.Executor
}

func (shell *ShellExecutor) Run(workDir string, command model.Command) model.ActionResult {
	commandName := "sh"
	if constants.OsType() == constants.OsWin {
		commandName = "powershell"
	}

	result := &model.RenderActionResult{}
	var args = make([]string, 0)
	args = append(args, command.Cmd)
	args = append(args, command.Args...)
	cmdExe := exec.Command(commandName, args...)

	stdout, err := cmdExe.StdoutPipe()
	if err != nil {
		log.GLog.Error("ShellExecutor: ", err)
		return result
	}
	cmdExe.Stderr = os.Stderr
	cmdExe.Dir = workDir
	err = cmdExe.Start()
	if err != nil {
		log.GLog.Error("ShellExecutor: ", err)
		return result
	}
	reader := bufio.NewReader(stdout)
	for {
		line, err2 := reader.ReadString('\n')
		if err2 != nil || io.EOF == err2 {
			break
		}
		fmt.Print(line)
	}
	err = cmdExe.Wait()
	return result
}

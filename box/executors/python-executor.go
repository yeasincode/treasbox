package executors

import (
	"bufio"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"treasbox/box/context"
	"treasbox/box/log"
	"treasbox/box/model"
)

type PythonExecutor struct {
	model.Executor
	ModuleName string //模块名test-executor.py,类名TestExecutor
}

func (shell *PythonExecutor) Run(workDir string, command model.Command) model.ActionResult {
	commandName := "python.exe"
	ctx := context.GetContext()

	result := &model.RenderActionResult{}
	var args = make([]string, 0)
	args = append(args, filepath.Join(ctx.ExecutorsDir, "\\python\\executor-host.py"))
	args = append(args, shell.ModuleName)
	args = append(args, command.Cmd)
	args = append(args, command.Args...)
	cmdExe := exec.Command(commandName, args...)

	stdout, err := cmdExe.StdoutPipe()
	if err != nil {
		log.GLog.Error("PythonExecutor: ", err)
		return result
	}
	//cmdExe.Path = ""
	cmdExe.Stderr = os.Stderr
	cmdExe.Dir = workDir
	err = cmdExe.Start()
	if err != nil {
		log.GLog.Error("PythonExecutor: ", err)
		return result
	}
	reader := bufio.NewReader(stdout)
	var pythonResult string
	for {
		line, err := reader.ReadString('\n')
		if err != nil || io.EOF == err {
			break
		}
		pythonResult += line
	}
	result.Msg = pythonResult
	err = cmdExe.Wait()
	return result
}

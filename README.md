# treas box

#### 介绍
treas box是工具宝库的意思，这是在工作中不想重复性工作而为具体的定制了命令行工具。
首先这个工具默认支持调用系统命令行工具，其次可以通过添加命令行指令[Executor]来增加命令指令，以此来简化工作。
同时这个工具由go语言开发，天生地支持跨平台。

#### 安装教程

1.  运行build.bat/.sh生成treas命令行可执行文件
2.  配置treas可执行文件及配置文件treas.yml到环境变量

#### 使用说明

1.  treas bundle prod 运行task集合
2.  treas task test 运行task

### 指令说明
break 加入命令执行的断点，可以观察之前执行的任务是否成功，决定任务是否继续执行

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

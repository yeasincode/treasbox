from executor import Executor, Result


class TestExecutor(Executor):
    """
    处理数据的接口
    """

    def run(self, cmd, *args):
        return Result(1, "String", "aa=>bb")

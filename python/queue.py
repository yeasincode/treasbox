import sqlite3
from threading import Lock

class Queue:
    _instance = None
    _lock = Lock()

    def __new__(cls, db_path):
        if not cls._instance:
            with cls._lock:
                if not cls._instance:
                    cls._instance = super(Queue, cls).__new__(cls)
                    cls._instance._init_db(db_path)
        return cls._instance

    def _init_db(self, db_path):
        self.conn = sqlite3.connect(db_path)
        self.cursor = self.conn.cursor()
        self._create_table()

    def _create_table(self):
        create_table_sql = """
        CREATE TABLE IF NOT EXISTS queue (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            value TEXT
        );
        """
        self.cursor.execute(create_table_sql)
        self.conn.commit()

    def enqueue(self, value):
        insert_sql = "INSERT INTO queue (value) VALUES (?)"
        self.cursor.execute(insert_sql, (value,))
        self.conn.commit()

    def dequeue(self):
        self.cursor.execute("BEGIN")
        select_sql = "SELECT id, value FROM queue ORDER BY id LIMIT 1"
        self.cursor.execute(select_sql)
        row = self.cursor.fetchone()
        if row:
            id, value = row
            delete_sql = "DELETE FROM queue WHERE id = ?"
            self.cursor.execute(delete_sql, (id,))
            self.conn.commit()
            return value
        else:
            self.conn.rollback()
            return None

    def peek(self):
        select_sql = "SELECT value FROM queue ORDER BY id LIMIT 1"
        self.cursor.execute(select_sql)
        row = self.cursor.fetchone()
        if row:
            return row[0]
        else:
            return None

    def close(self):
        self.conn.close()

def get_queue_instance(db_path):
    return Queue(db_path)


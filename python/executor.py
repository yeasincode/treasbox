import abc


class Executor:
    """
    处理数据的接口
    """

    @abc.abstractmethod
    def run(self, cmd, *args):
        pass


class Result:
    def __init__(self, typ, name, data):
        # 1.render 2. stack
        self.typ = typ
        # 类型为数据时，这个为具体序列化的类；类型为函数时，这个为函数名，参数则为数据部分
        self.name = name
        self.data = data

    def __json__(self):
        return {
            "typ": self.typ,
            "name": self.name,
            "data": self.data
        }

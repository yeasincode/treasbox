import importlib
import sys
import json


def pascalCase(snake_str):
    """
    转大驼峰
    """
    # 以连字符分隔字符串
    words = snake_str.split('-')
    # 将每个单词的首字母大写
    pascal_words = [word.capitalize() for word in words]
    # 连接这些单词
    pascal_case_str = ''.join(pascal_words)
    return pascal_case_str


fileName, moduleName, cmdName, *args = sys.argv
className = pascalCase(moduleName)
module = importlib.import_module(moduleName, ".")
cls = getattr(module, className)
instance = cls()
data = instance.run(cmdName, *args)
resultJson = json.dumps(data, default=lambda o: o.__json__())
print(resultJson)
